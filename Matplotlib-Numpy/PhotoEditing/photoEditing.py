# # Photo editing
# 
# Warning: We want to solve the following questions with a **vectorization approach**. 
# At no time should you explicitly call a loop! 
# To each question we give "useful functions", they are necessary 
# but not always sufficient.
# 
# The matplotlib library allows us to read images in png format and 
# returns a three-dimensional array that contains the intensity of each color 
# of each pixel. The intensity is a value between 0 and 255.
# - dimension 1 is the absciss of a pixel;
# - dimension 2 is the ordinate;
# - dimension 3 is the color index, in RGB format. 
# 
# We have chosen a raccoon image that allows us to correctly visualize 
# the treatment that we operate. After testing your functions on it, you can use 
# other png images. Download it and place it into your working directory.
# 
# <img src="raccoon.png">
# 
# Warning: depending on your computer (and the libraries installed), 
# `imread` may have a slightly different behavior. In particular :
# - it can return an array of int (between 0 and 255) or an array of float 
# (between 0. and 1.);
# - the last dimension of the array can be of dimension 3 or 4 (in that case, 
# each pixel is encoded in the format RGBA: red, green, blue and transparency). 
# 
# The rest of the questions suppose you have an array of int and the last 
# dimension is 3. If not, you can uncomment the two dedicated lines of the 
# following code to deal with each problem. 

import matplotlib.pyplot as plt
import numpy as np

raccoonImg = plt.imread("raccoon.png")

print(raccoonImg[10,10])
# If the result is an array of float, convert to int
# by uncommenting the following line
raccoonImg = raccoonImg * 255  

raccoonImg = raccoonImg.astype(int)
assert (raccoonImg[10,10] == [172, 169, 188, 255]).all()

print(raccoonImg.shape[2])
# If the result is 4, remove the transparency component
# by uncommenting the following line
raccoonImg = raccoonImg[:,:,0:3]

assert raccoonImg.shape[2] == 3

plt.imshow(raccoonImg, norm=None)
plt.show()


# ## Grayscale
# 
# ### Conversion
# Write a function `grayscale(img)` which takes as a parameter a color image 
# in the form of an array and returns a new array of only 2 dimensions, 
# containing the gray level of each pixel.
# 
# Our eyes do not perceive the luminous intensity of each color in the same way, 
# it is necessary to apply the following coefficients for the conversion: 
# red 30%, green 59% and blue 11%.
# 
# Here are some useful functions / operations:
# - `A * x`, the operation multiplies each value of `A` by the scalar `x`;
# - `A.astype(int)`, creates a copy of the array `A` containing only rounded values; 
# - `np.zeros(n)` to create a single dimension array of size n;
# - `plt.imshow(img, cmap="gray")` displays a grayscale image. 
"""
def grayscale(img):
    raise NotImplementedError

print("image en niveaux de gris")
raccoonGrayImg = grayscale(raccoonImg)

assert raccoonGrayImg.shape == (768, 1024)
assert raccoonGrayImg[10,10] == 171

plt.imshow(raccoonGrayImg, cmap="gray", norm=None)
plt.show()
"""


# ### Histogram
# 
# Write a function `histogram(grayImg)` which takes a grayscale image as a parameter 
# and returns an histogram of the values of the intensities present in the image. 
# The return value is therefore a NumPy array of 256 values between 0 and 1, 
# representing the proportion of pixels having a certain value.
#  
# Here are some useful functions:
# - `A / x` the operation of dividing, in place, each value of `A` by the scalar `x`;
# - `np.add.at(A, indices, x)` for each value `i` in the array `indices`, increments by `x` the `i`*-th* value of `A`; it is equivalent to something like
#     ```
#     for i in indices:
#         A[i] += x
#     ```
#     but faster.
# - `np.sum(A)` sums the list of elements in A. 
"""
def histogram(grayImg):
   raise NotImplementedError
    
histo = histogram(raccoonGrayImg)
assert histo[10] == 0.002086639404296875
"""


# ### Plot histogram
# 
# Write a function `plot_intensity(grayImg)` that displays a gray image and 
# the histogram of the intensities side by side.
# 
# <img src="histogramme.png">
# 
# Here are some useful functions:
# - `np.linspace(0, 255, 256)` creates a new array with 256 values 
#    evenly distributed between 0 and 255 (inclusive);
# - `plt.fill_between(x, y)` to plot a filled solid curve;
# - `plt.figure(figsize = (15, 5))` to create a multiple graph of a given size;
# - `add_subplot(nbr, nbc, num)` to indicate the location of the next curve to plot,
#    i.e. in a grid of *nbr* lines and *nbc* column write in the *num* box (by 
#    reading order left-right, top-bottom). Example of subplot: 
#    https://matplotlib.org/gallery/subplots_axes_and_figures/axes_margins.html#sphx-glr-gallery-subplots-axes-and-figures-axes-margins-py
# 
"""
def plot_intensity(grayImg):
    raise NotImplementedError
    
plot_intensity(raccoonGrayImg)
"""


# ## Contrast
# 
# A photo is considered to be correctly exposed if the intensities are well 
# distributed over the whole spectrum, in particular, the black must be deep 
# and the white very bright. Many image editing softwares allow us to automatically 
# choose the contrast and the brightness in order to obtain a balanced image.
# 
# A naive approach to achieve this operation consists in transforming the 2% of 
# the lowest intensities in absolute black (value of 0) and the 2% of the strongest 
# light intensities in a saturated white (value of 255). The intermediate values 
# ​​are distributed linearly.
# 
# The first step, therefore, consists in finding the values ​​𝑝2 and 𝑝98 from the 
# 2nd and 98th centiles. In other words, 𝑝2 is the intensity such that 2% of the 
# total intensities are lower. The values ​​𝑝2 and 𝑝98 are between 0 and 255.
# 
# Once these values ​​are known, it is possible to solve the following equation system, 
# with 𝐶 and 𝐼 the contrasts and intensity corrections:
# 𝐶∗𝑝2+𝐼=0
# 𝐶∗𝑝98+𝐼=255
# 
# Once this system is solved, it is possible to correct each value 𝑖 of intensity 
# of the grayscale image by the new intensity : 𝐶∗𝑖+𝐼.
# 
# ### Centiles
# 
# Write a function `centiles(grayImg)` which, given a grayscale image, computes 
# and returns the 2nd and 98th centiles.
# 
# Exceptionally, you can use a for loop on the histogram. 
"""
def centiles(grayImg):
    raise NotImplementedError

assert centiles(raccoonGrayImg) == (11, 213)
"""


# ### Solving the system
# 
# NumPy provide a module to solve linear systems of equations. They must be in 
# matrix form.
#    ( p2    1 )   ( C )   (  0  )
#    (         ) * (   ) = (     )
#    ( p98   1 )   ( I )   ( 255 ) 
# <img src="equation.png">
# 
# Write a function `find_corrections(grayImg)` that returns the contrast and 
# intensity corrections to be made.
# 
# Here are the helpful instructions:
# ```
#     from numpy import linalg
#     X = linalg.solve(A, B) with AX = B 
# ```
"""
from numpy import linalg

def find_corrections(grayImg):
    raise NotImplementedError

C, I = find_corrections(raccoonGrayImg)
assert C == 1.2623762376237622
assert I == -13.886138613861387
"""


# ### Auto constrast
# 
# Write a function `auto_contrast(grayImg)` that takes a grayscale image as 
# a parameter and returns a new image with the application of the automatic 
# contrast algorithm.
# 
# <img src="auto-contraste.png">
# 
# Here are some useful functions:
# - `np.zeros_like(A)` creates a new array of the same shape and size as `A` 
#    and containing zeros;
# - `np.clip(A, min, max)` creates a new array where values in `A` less than *min* 
#    are replaced by *min* and those greater than *max* are replaced by *max*. 
"""
def auto_contrast(grayImg):
    raise NotImplementedError

corrected = auto_contrast(raccoonGrayImg)
plot_intensity(raccoonGrayImg)
plot_intensity(corrected)
"""


# ### Well done!
# 
# You have completed the first part of this exercise. Now you know how NumPy 
# views work and how to use NumPy to avoid explicit loops. If you are ahead of 
# your group, we advise you to continue with these few optional exercises which 
# will allow you to tackle the problem of the intensity of the color image. 

# ## Optional questions - Color image

# ### Color histogram
# 
# Write a function `plot_color_intensities(img)` which displays an image and 
# next to it, the 3 intensity histograms (R, G and B) superposed. We can use the 
# function `histogram(img)` that we previously created to retrieve the intensity 
# histogram of a grayscale image and apply it to a view of each color.
# 
# Useful function:
# - `plt.fill_between(x, y, color="#FF000055")` to display a filled solid curve, 
#    here red with transparency (see hexadecimal notation of color codes). 
"""
def plot_color_intensities(img):  
    raise NotImplementedError

plot_color_intensities(raccoonImg)
"""


# ### Color model conversion
# 
# Our image is currently encoded with the RGB color model. In this model the 
# intensity is "distributed" over each of the three colors. We could imagine an 
# auto-contrast algorithm on color images by working on each intensity independently. 
# By doing so, we take the risk of changing the hue of an image.
# 
# For auto contrast it is better to change the color model, for example the YCbCr model.
# In this model, each pixel is represented by three components. The first Y is 
# intensity (0 for black, 255 for white). The second and third components represent 
# the variation in hue.
# 
# We can switch from the RGB model to the YCbCr model:
#   ( Y  )   (  0  )   (   0.299       0.587      0.114  )   ( R )
#   ( Cb ) = ( 128 ) + ( -0.168736  -0.331264       0.5  ) * ( G )
#   ( Cr )   ( 128 )   (    0.5     -0.418688  -0.081312 )   ( B )
# <img src="equation_color_1.png">
# 
# and conversely
#   ( R )   ( 1       0          1.402  )   (    Y     )
#   ( G ) = ( 1  -0.344136    -0.714136 ) * ( Cb - 128 )
#   ( B )   ( 1    1.772           0    )   ( Cr - 128 )
# <img src="equation_color_2.png">
# 
# Write two functions `to_YCbCr(img)` and `to_RGB(img)` to switch from one model to 
# another. Also write a function `test_convertion(img)` which checks that everything 
# is in order (if this is the case, the difference between each pixel of an image 
# *img* and *to_RGB(to_YCbCr(img))* should be small (less than 1 if you forget that 
# the values ​​are integer, a few units otherwise).
# 
# Tips: The matrix to convert a pixel is of dimension (3, 3). However, an image is of 
# dimension (rows, cols, 3). [The NumPy documentation says about the `np.dot(a, b)` 
# product function](https://numpy.org/doc/stable/reference/generated/numpy.dot.html): 
# *"If a is an N-D array and b is an M-D array (where M>=2), it is a sum product 
# over the last axis of a and the second-to-last axis of b"*. 
# The simplest solution is therefore to reverse the order of the operands of the 
# product and therefore to transpose the conversion matrix.
# 
# Here are some useful functions:
# - `np.dot(A, B)`: product;
# - `A.T` is the transpose of A;
# - `A.astype(int)` builds an array with rounded values of `A`;
# - [`np.clip(A, 0, 255)`](https://numpy.org/doc/stable/reference/generated/numpy.clip.html?highlight=clip#numpy.clip) builds an array with the values ​​in the interval [0,255] (values smaller than 0 become 0, values larger than 255 become 255);
# - `np.absolute(A)` builds an array with the absolute values ​​of `A`.
"""
def to_YCbCr(img):
    raise NotImplementedError

assert (to_YCbCr(raccoonImg)[10,10] == [172, 136, 127]).all()

def to_RGB(img):
    raise NotImplementedError

for i in range(3):
    assert to_RGB(to_YCbCr(raccoonImg))[10,10,i] > raccoonImg[10,10,i]-4
    assert to_RGB(to_YCbCr(raccoonImg))[10,10,i] < raccoonImg[10,10,i]+4

def test_convertion(img):
    raise NotImplementedError
    
test_convertion(raccoonImg)    
"""


# ### Color auto constrast
# 
# Write a function `auto_contrast_YCbCr(img)` which converts *img* in YCbCr mode, 
# applies auto contrast to the *Y* component, then returns the converted image in 
# RGB mode.
"""
def auto_contrast_YCbCr(img):
    raise NotImplementedError

plot_color_intensities(raccoonImg) 
plot_color_intensities(auto_contrast_YCbCr(raccoonImg))
"""